﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public float speed;

	public AudioClip redClip;
	public AudioClip greenClip;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		transform.Translate(new Vector3(h, v, 0) * speed);

	}
	void OnTriggerEnter2D(Collider2D collider) {
		Debug.Log(collider.gameObject.name);
		if (collider.gameObject.tag == "Red") {
			// play red sound
			AudioSource player = GetComponent<AudioSource>();
			player.PlayOneShot(redClip);
		} else if (collider.gameObject.tag == "Green") {
			// play red sound
			AudioSource player = GetComponent<AudioSource>();
			player.PlayOneShot(greenClip);
		}
	} 

}
